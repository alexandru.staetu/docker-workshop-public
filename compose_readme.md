###### Version
```yaml
version: '3.4'
```

##### Docker services

```bash
docker service create --name redis --replicas=5 redis:3.0.6
```

```bash
cd service_discovery
docker-compose up -d
docker-compose ps
docker exec -it servicediscovery_static_1 sh 
> apk update && apk add bind-tools
> dig scaled-service
> exit

docker-compose up --scale scaled-service=10 -d
docker-compose ps
docker exec -it servicediscovery_static_1 sh 
> dig scaled-service
```

##### Using a dockerfile or an image
```bash
cd compose_build_and_image
docker-compose up -d
docker ps --filter "name=composebuildandimage_using"
```

##### Using a host volume

```bash
cd compose_host_volumes
docker-compose up
ls data_volume
```

##### Using a named volume

```bash
cd compose_volumes
docker-compose --f docker-compose-write.yaml up
docker-compose --f docker-compose-read.yaml up
```

##### Service port mapping

```bash
cd compose_ports
docker-compose ps
docker-compose up -d
curl localhost:8080
```

##### Network

```bash
cd compose_network
docker-compose up -d
ping 192.168.111.222 -c 1

docker network ls
docker network inspect composenetwork_compose_network

docker ps
docker inspect composenetwork_static-networked_1
```
##### Variables

```bash
cd compose_variables
docker-compose up -d
docker-compose exec env-variables env
```

##### Resources

```bash
cd compose_resources
docker-compose up -d
ps aux|grep yes
docker-compose down
```

